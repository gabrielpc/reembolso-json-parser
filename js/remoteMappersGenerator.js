var extensions = []
var currentExtension = 0

var RMMappersCode = ""
var isFirstMapper = true

function generateRemoteMappersRecursive(obj, prevName) {
    var mainClassName = document.getElementById("mainClassName").value
    RMMappersCode += "\n\n\
extension " + mainClassName + "RM { \n\
\tfunc toDomainModel() -> " + mainClassName + " { \n\
\t\treturn " + mainClassName + "("
generateRemoteMappersConstructors(obj, prevName)

    RMMappersCode += ")\n\t}\n}"

    for (var i = 0; i < keysCountOf(obj); i++) {
        var nextLevel = obj[keyAt(i,obj)];

        if (typeof valAt(i,obj) == "object") {
            
            isFirstMapper = false
            var keyName = modifyKeyNameIfIsAnArrayPosition(keyAt(i,obj), prevName)
            var className = getTranslation(keyName)
           
           
            RMMappersCode += "\n\n\
extension " + mainClassName + "RM." +  className + "RM { \n\
\tfunc toDomainModel() -> " + mainClassName + "." + className + " { \n\
\t\treturn " + mainClassName + "." + className + "("
            if (isArray(nextLevel)) {
                if (typeof nextLevel[0] == "object") {
                    generateRemoteMappersConstructors(valAt(i,obj)[0], prevName)
                }
            } else {
                generateRemoteMappersConstructors(valAt(i,obj), prevName)
            }

            RMMappersCode += ")\n\t}\n}"

            if (isArray(nextLevel)) {
                if (typeof nextLevel[0] == "object") {
                    nextLevel = nextLevel[0]
                    RMMappersCode += "\n\n\
extension Array where Element == " +  mainClassName + "RM." + className + "RM { \n\
\tfunc toDomainModel() -> [" +  mainClassName + "." + className + "] { \n\
\t\treturn self.map({ $0.toDomainModel() })\n\
\t}\n\
}"
                }
            } 
            if (typeof nextLevel[0] == "object") {
                generateRemoteMappersRecursive(nextLevel, keyName)
            }
            
        }
    }
}

function generateRemoteMappersConstructors(obj, prevName) {
    for (var i = 0; i < keysCountOf(obj); i++) {
        var keyName = modifyKeyNameIfIsAnArrayPosition(keyAt(i,obj), prevName)
        var varType = getSwiftType(valAt(i, obj),keyName)
        var nextLevel = obj[keyAt(i,obj)];

        var varName = camelCase(getTranslation(keyName))
        RMMappersCode += varName + ": " + varName

        if (isArray(nextLevel) && (typeof nextLevel[0] == "object")) {
            RMMappersCode += ".toDomainModel()"
        }
        if (i < keysCountOf(obj) - 1) {
            RMMappersCode += ", "
        }
    }
}