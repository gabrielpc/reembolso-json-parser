
function httpGet(theUrl)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", theUrl, false ); // false for synchronous request
    xmlHttp.send( null );
    if (xmlHttp.status == 429) {
        alert("Número de traduções no Google Tradutor excedidas!\nAguarde um momento e tente mais tarde.")
    }
    return xmlHttp.responseText;
}

function translate(phrase) {
    var response = httpGet("https://translate.googleapis.com/translate_a/single?client=gtx&sl=pt&tl=en&dt=t&q=" + encodeURI(phrase))
    return JSON.parse(response)[0][0][0]
}


function translateVariableNames() {
    var stringToTranslate = ""
    for (var i = 0; i < variableNames.length; i++) {
        let tranlatableVariableName = toTranslatablePhrase(variableNames[i])

        if (tranlatableVariableName == "Carteirinha") {
            tranlatableVariableName = "Cartao Segurado"          
        }
        if (tranlatableVariableName == "Serie") {
            tranlatableVariableName = "Numero de Serie"          
        }

        if (encodeURI(stringToTranslate).length + 2 + encodeURI(tranlatableVariableName).length > 150) {
            runAndstoreTranslation(stringToTranslate.slice(0,-2))
            stringToTranslate = ""
        }
        stringToTranslate += tranlatableVariableName

        if (i == variableNames.length - 1) {
            runAndstoreTranslation(stringToTranslate)
        }
        stringToTranslate += ", "
    }
}

function runAndstoreTranslation(stringToTranslate) {
    //translatedVariablesString = translate(stringToTranslate)
    translatedVariablesString = stringToTranslate
    //console.log(stringToTranslate)
    //console.log(translatedVariablesString)

    if (translatedVariablesString == undefined) {
        console.log("Translation Failed.")
        translatedVariablesString = stringToTranslate;
    }
    var translatedVariableNamesArray = translatedVariablesString.split(",")

    for (var i = 0; i < translatedVariableNamesArray.length; i++) {
        var k = currentOriginalWordIdx + i;
        let trVar = removeSpaces(translatedVariableNamesArray[i]);
        if (trVar == "Protocol") {
            trVar += "Number"
        }
        translation[variableNames[k]] = trVar;
    }
    currentOriginalWordIdx += translatedVariableNamesArray.length;
}