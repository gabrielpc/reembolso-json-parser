var swiftCode = ""
var jsonCode =  ""

var JSONCodeTextArea
var mainClassNameInputBox

var variableNames = []
var translation = []

var currentOriginalWordIdx = 0
var firstLevel = true


$(document).ready(function(){
   openTab(undefined, 'modelsTab')
   document.getElementById("defaultTab").classList.add("active")

    document.getElementById('button').onclick = function(){
        swiftCode = ""
        RMMappersCode = ""
        variableNames = []
        translation = []
        currentOriginalWordIdx = 0
        firstLevel = true
        var obj
        var mainClassObj

        try {
            obj = JSON.parse(document.getElementById("jsonCodeTextArea").value);
        } catch (err) {
            alert("JSON Parsing error")
        }
  
    
        if (keyAt(0,obj) == "codigo" || keyAt(0,obj) == "resultado"){ 
            for (var i = 0; i < keysCountOf(obj); i++) {
                var keyName = keyAt(i,obj).toLowerCase()
                if (keyName != "codigo" && keyName != "descricao" && keyAt(0,obj) != "resultado") {
                    mainClassObj =  obj[keyName]
                    break
                }
            }
        } else {
            mainClassObj = obj
        }
    
        getVariableNamesRecursive(mainClassObj, ""); 
        translateVariableNames()
       
        generateVariablesRecursive(mainClassObj, ""); 

        generateRemoteMappersRecursive(mainClassObj, "");
        
        updatedSwiftCode() 
    }
});

function updatedSwiftCode() {
    var header =  "struct " + removeSpaces(document.getElementById("mainClassName").value) + "RM: Codable "
    document.getElementById("modelsTextArea").value  = header + swiftCode +  "}\n"

    document.getElementById("mappersTextArea").value = RMMappersCode
}

function generateVariablesRecursive(obj, prevName) {
   
    generateVariables(obj, prevName)

    for (var i = 0; i < keysCountOf(obj); i++) {
        if (typeof valAt(i,obj) == "object") {
            var keyName = modifyKeyNameIfIsAnArrayPosition(keyAt(i,obj), prevName)
            var className = getTranslation(keyName)
            var nextLevel = obj[keyAt(i,obj)];
            if (isArray(nextLevel)) {
                if (typeof nextLevel[0] == "object") {
                    nextLevel = nextLevel[0]
                    swiftCode += "\n"
                    indent()
                    swiftCode += "struct " + className + "RM: Codable "
                    generateVariablesRecursive(nextLevel, keyName)
                }
                
            } else {
                swiftCode += "\n"
                indent()
                swiftCode += "struct " + className + "RM: Codable "
                generateVariablesRecursive(nextLevel, keyName)
            }
        }
    }
   
}

function generateVariables(obj, prevName) {
    swiftCode += "{\n"
    for (var i = 0; i < keysCountOf(obj); i++) {
        var keyName = modifyKeyNameIfIsAnArrayPosition(keyAt(i,obj), prevName)
        var varType = getSwiftType(valAt(i, obj),keyName)
        var nextLevel = obj[keyAt(i,obj)];
    
        if (isArray(nextLevel)) {
            if (typeof nextLevel[0] != "object") {
                varType = getSwiftType(nextLevel[0],keyName)
            }
            varType = "[" + varType + "]"
        }
        indent()
        swiftCode += "\tlet " + camelCase(getTranslation(keyName)) + ": " + varType + "\n"
    }
    swiftCode += "\n"
    indent()
    swiftCode += "\tenum CodingKeys: String, CodingKey {\n"
    for (var i = 0; i < keysCountOf(obj); i++) {
        var keyName = modifyKeyNameIfIsAnArrayPosition(keyAt(i,obj), prevName)
        indent()
        swiftCode += "\t\tcase " + camelCase(getTranslation(keyName)) + " = \"" + keyAt(i,obj) + "\"\n"
    }
    indent()
    swiftCode += "\t}\n"
    if (firstLevel) {
        firstLevel = false
    } else {
        indent()
        swiftCode += "}\n"
    }
}

function indent() {
    if (!firstLevel) {
        swiftCode += "\t"
    }
}