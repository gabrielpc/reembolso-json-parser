
function openTab(evt, tabName) {
    // Declare all variables
    var i, tabcontent, tablinks;
  
    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
  
    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
  
    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(tabName).style.display = "block";

    if (evt != undefined) {
        evt.currentTarget.className += " active";
    }
}

function camelCase(word) {
    return word[0].toLowerCase() + word.slice(1)
}

function getTranslation(variable) {
    var translatedVar = translation[variable];
    if (translatedVar == undefined) {
        console.log("Failed to translate: \'" + variable + "\'");
        return variable;
    }
    return translatedVar
}

function getSwiftType(value, keyName) {
    var result = "";
    if (typeof value == "string") {
        return "String";
    }
    else if (typeof value == "boolean") {
        return "Bool";
    } 
    else if (typeof value == "number") {
       if (value % 1 != 0) {
           return "Double";
       } else {
           return "Int";
       }
    }
    return getTranslation(keyName) + "RM"
}

function modifyKeyNameIfIsAnArrayPosition(keyName, prevName) {
    if (keyName.length < 2) {
        if (prevName.slice(-1) == 's') {
            prevName = prevName.slice(0, -1);
        }
        keyName = prevName + "Item"
    }
    return keyName
}

function toTranslatablePhrase(keyName) {
    var separatedWordsPhrase = keyName[0].toUpperCase()
    for (var i = 1; i < keyName.length; i++) {
        var c = keyName[i]
        if (c == c.toUpperCase()) {
            separatedWordsPhrase += " "
        } 
        separatedWordsPhrase += c
    }
    return separatedWordsPhrase
}


function keyAt(idx,obj)  {
    return Object.keys(obj)[idx]
}

function valAt(idx, obj)  {
    return obj[Object.keys(obj)[idx]]
}

function keysCountOf(obj) {
    return Object.keys(obj).length
}

function isArray(obj) {
    return (typeof obj == "object" && obj.length != undefined)
}

function removeSpaces(str) {
    var result = "";
    for (var i = 0; i < str.length; i++) {
        if (str[i] != " ") {
            result += str[i];
        }
    }
    return result;
}
