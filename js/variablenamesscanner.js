

function getVariableNamesRecursive(obj, prevName) {
   
    getVariableNames(obj, prevName)

    for (var i = 0; i < keysCountOf(obj); i++) {
        if (typeof valAt(i,obj) == "object") {
            var keyName = modifyKeyNameIfIsAnArrayPosition(keyAt(i,obj), prevName)
            var nextLevel = obj[keyAt(i,obj)];
            if (nextLevel[0] != undefined) {
                if (typeof nextLevel[0] == "object") {
                    nextLevel = nextLevel[0];
                    getVariableNamesRecursive(nextLevel, keyName)
                }
            } else {
                getVariableNamesRecursive(nextLevel, keyName)
            }
        }
    }
   
}

function getVariableNames(obj, prevName) {
    for (var i = 0; i < keysCountOf(obj); i++) {
        var keyName = modifyKeyNameIfIsAnArrayPosition(keyAt(i,obj), prevName)
        variableNames.push(keyName)
    }
}